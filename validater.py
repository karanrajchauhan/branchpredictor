import os
import pandas as pd
import subprocess

#========================= CREATE RESULT OUTPUT FILES =========================#

# directory from which to load
# TODO: MUST BE CHANGED IF NOT MY OWN MACHINE
target_dir = '/home/karanraj/Documents/shared/ec513/labhandouts/lab2handout/bpredictor.cpp'

# cross validate history length from 4 to 32
for hist_len in range(4, 33):

	# main content with test cases to be put in files
	template_content = []
	with open('template_file.cpp', 'r') as f_handler:
		for line in f_handler.readlines():
			if line.__contains__("#define HISTORY_LEN "):
				template_content.append("#define HISTORY_LEN " + str(hist_len) + line[-1])
			else:
				template_content.append(line)

	# create new file with current value of history length
	with open(target_dir, 'w') as f_handler:
		f_handler.writelines(template_content)

	try:
		# run make
		print("running make")
		p = subprocess.Popen(["make"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		p.communicate()

		# run tests and save output
		print("running tests")
		with open(str(hist_len)+".txt", "w") as f:
			subprocess.call(["./lab2test.pl"], stdout=f)
		print("output file successfully created")

	except FileNotFoundError:
		print('Executable not found')

	except IsADirectoryError:
		print('Not a valid directory')

	except:
		print('unknown error')


#============================= CREATE SUMMARY CSV =============================#

output_files = [file for file in os.listdir("./") if file.endswith(".txt")]

df = pd.DataFrame(index=output_files, columns=range(6))

for out_file in output_files:

	col_idx = 0
	with open(out_file, 'r') as f_handler:
		for line in f_handler.readlines():
			if line.__contains__("CCR = "):
				df.loc[out_file][col_idx] = eval(line[6:-1])
				col_idx += 1

# get average ccr for each history length
df['Average'] = df.mean(axis=1)
df.to_csv("cross_val.csv")
