Software simulation of perceptron based branch predictor

Also included: one-bit, two-bit branch predictors

The validater script (validater.py) is used to cross validate for hyperparamters of the perceptron based predictor.
It uses the template file (template_file.cpp) and copies the code inside it, along with the updated paramters (the ones to be tested) to the bpredictor.cpp, then runs make and then tests
The results of cross validation are stored in cross_val.csv
