#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <fstream>
#include <time.h>
#include "pin.H"

// predictor parameters, utitlizing all possible space
#define HISTORY_LEN 19
#define THETA 50
#define PERCEPTRON_SIZE 20
#define NUM_PERCEPTRONS 51

// prediction statistics
static UINT64 takenCorrect = 0;
static UINT64 takenIncorrect = 0;
static UINT64 notTakenCorrect = 0;
static UINT64 notTakenIncorrect = 0;

class BranchPredictor
{
public:
    BranchPredictor() { }
    virtual INT32 makePrediction(ADDRINT insAddress) { return 1;};
    virtual VOID makeUpdate(BOOL didTakeBranch, BOOL didPredictTaken, INT32 y, ADDRINT insAddress) {};
};

// NOT BEING INSTANTIATED, JUST PUT HERE BECAUSE THIS IS ALSO A DECENT-ISH PREDICTOR
class OneBitBP: public BranchPredictor
{
public:

    OneBitBP()
    {
        // initialize all to true
        for (UINT32 i = 0; i < branchTableSize; ++i)
        {
            didInsBranchTable[i] = TRUE;
        }
        printf("Using one bit predictor with table size = %d\n", branchTableSize);
    }

    // do the same thing as last time
    BOOL makePrediction(ADDRINT insAddress)
    {
        return didInsBranchTable[ (insAddress % branchTableSize) ];
    };

    // if incorrect prediction then update bool with current ground truth
    VOID makeUpdate(BOOL didTakeBranch, BOOL didPredictTaken, ADDRINT insAddress)
    {
        if (didTakeBranch != didPredictTaken)
        {
            didInsBranchTable[ (insAddress % branchTableSize) ] = didTakeBranch;
        }
    }
};

// NOT BEING INSTANTIATED, JUST PUT HERE BECAUSE THIS IS ALSO A DECENT-ISH PREDICTOR
class TwoBitBP: public BranchPredictor
{
    // Using grey code to encode states. This reduces number of comparisons
    //      10  -   strongly taken
    //      11  -   weakly taken
    //      01  -   weakly not taken
    //      00  -   strongly not taken

public:

    TwoBitBP()
    {
        // initialize to weakly taken
        for (UINT32 i = 0; i < branchTableSize; ++i)
        {
            branchTableBit0[i] = TRUE;
            branchTableBit1[i] = TRUE;
        }
        printf("Using two bit predictor with table size = %d\n", branchTableSize);
    }

    // return true if in either weakly taken or strongly taken state
    BOOL makePrediction(ADDRINT insAddress)
    {
        return branchTableBit1[ (insAddress % branchTableSize) ];
    };

    // if incorrect prediction then update bool with current ground truth
    VOID makeUpdate(BOOL didTakeBranch, BOOL didPredictTaken, ADDRINT insAddress)
    {
        // correct prediction
        if (didTakeBranch == didPredictTaken)
        {
            // make bit0 = 0 i.e. if already in strong state then no diff, otherwise go to strong state
            branchTableBit0[ (insAddress % branchTableSize) ] = FALSE;
        }
        // incorrect prediction
        else
        {
            // currently in a weak state
            if ( branchTableBit0[ (insAddress % branchTableSize) ] )
            {
                // go from taken to not taken - or not taken to taken
                branchTableBit1[ (insAddress % branchTableSize) ] = !branchTableBit1[ (insAddress % branchTableSize) ];
            }
            else
            {
                // go to weak state
                branchTableBit0[ (insAddress % branchTableSize) ] = TRUE;
            }
        }
    }
};

class PerceptronBP: public BranchPredictor
{
public:

    PerceptronBP()
    {
        // Initialize history reg to all takens
        for (UINT32 i = 0; i < HISTORY_LEN; ++i)
        {
            globalHistoryReg[i] = 1;
        }

        // rand num mod with modee will be in range [0, THETA]
        INT32 modee = THETA + 1;

        // Initialize weights to random numbers between [-THETA/2, THETA/2]
        for (UINT32 pNum = 0; pNum < NUM_PERCEPTRONS; ++pNum)
        {
            for (UINT32 i = 0; i < PERCEPTRON_SIZE; ++i)
            {
                perceptrons[pNum][i] = (rand() % modee) - THETA/2;
            }
        }

        printf("INITIALIZED PERCEPTRON BASED PREDICTOR\n");
        std::cout << "HISTORY_LEN = " << HISTORY_LEN << "\n";
        std::cout << "THETA = " << THETA << "\n";
        std::cout << "PERCEPTRON_SIZE = " << PERCEPTRON_SIZE << "\n";
        std::cout << "NUM_PERCEPTRONS = " << NUM_PERCEPTRONS << "\n";
    }

    INT32 makePrediction(ADDRINT insAddress)
    {
        INT32 y;

        // select perceptron to use
        UINT32 perceptron_idx = (insAddress % NUM_PERCEPTRONS);

        // init to w0*1
        y = perceptrons[perceptron_idx][0];

        // compute y as dot prod of wts and history reg
        for (UINT32 i = 0; i < HISTORY_LEN; ++i)
        {
            // perceptron wt index +1 bcoz w0 already included.
            y += perceptrons[perceptron_idx][i+1] * globalHistoryReg[i];
        }

        return y;
    }

    VOID makeUpdate(BOOL didTakeBranch, BOOL didPredictTaken, INT32 y, ADDRINT insAddress)
    {
        // select perceptron to train
        UINT32 perceptron_idx = (insAddress % NUM_PERCEPTRONS);

        // update weights if abs(y)<THETA or incorrect prediction
        if ( (didTakeBranch != didPredictTaken) || (abs(y) <= THETA) )
        {
            // update bias i.e. w0
            perceptrons[perceptron_idx][0] += (2*didTakeBranch - 1);

            // update other weights
            for (UINT32 i = 1; i < PERCEPTRON_SIZE; ++i)
            {
                // idx for hist reg -1 bcoz x1 is at idx 0
                perceptrons[perceptron_idx][i] += (2*didTakeBranch - 1) * globalHistoryReg[i-1];
            }
        }

        // shift and writeback to history reg
        for (UINT32 i = HISTORY_LEN; i > 0; --i)
        {
            globalHistoryReg[i] = globalHistoryReg[i-1];
        }
        globalHistoryReg[0] = didTakeBranch ? 1 : -1;
    }

private:
    // init reg as bool array not int array bcoz constraint is on memory not time/computation
    int8_t globalHistoryReg[HISTORY_LEN];

    // table of perceptrons
    INT32 perceptrons[NUM_PERCEPTRONS][PERCEPTRON_SIZE];
};


BranchPredictor* BP;

// This knob sets the output file name
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "result.out", "specify the output file name");


// In examining handle branch, refer to quesiton 1 on the homework
VOID handleBranch(ADDRINT ip, BOOL direction)
{
    INT32 yValue = BP->makePrediction(ip);
    BOOL prediction = yValue>=0;

    BP->makeUpdate(direction, prediction, yValue, ip);
    if(prediction)
    {
        if(direction)
        {
            takenCorrect++;
        }
        else
        {
            takenIncorrect++;
        }
    }
    else
    {
        if(direction)
        {
            notTakenIncorrect++;
        }
        else
        {
            notTakenCorrect++;
        }
    }
}

VOID instrumentBranch(INS ins, void * v)
{
    if(INS_IsBranch(ins) && INS_HasFallThrough(ins))
    {
        INS_InsertCall(
            ins, IPOINT_TAKEN_BRANCH, (AFUNPTR)handleBranch,
            IARG_INST_PTR,
            IARG_BOOL, TRUE,
            IARG_END);

        INS_InsertCall(
            ins, IPOINT_AFTER, (AFUNPTR)handleBranch,
            IARG_INST_PTR,
            IARG_BOOL, FALSE,
            IARG_END);
    }
}


/* ===================================================================== */
VOID Fini(int, VOID * v)
{
    printf("CCR = %lu / %lu\n", (takenCorrect+notTakenCorrect), (takenCorrect+takenIncorrect+notTakenCorrect+notTakenIncorrect));
  ofstream outfile;
  outfile.open(KnobOutputFile.Value().c_str());
  outfile.setf(ios::showbase);
  outfile << "takenCorrect: "<< takenCorrect <<"  takenIncorrect: "<< takenIncorrect <<" notTakenCorrect: "<< notTakenCorrect <<" notTakenIncorrect: "<< notTakenIncorrect <<"\n";
  // outfile << "accuracy = " << (takenCorrect+notTakenCorrect) << "/" << (takenCorrect+takenIncorrect+notTakenCorrect+notTakenIncorrect) << "\n";
  outfile.close();
}


// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    // rng seed
    srand( time(NULL) );

    // Make a new branch predictor
    BP = new PerceptronBP();

    // Initialize pin
    PIN_Init(argc, argv);

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(instrumentBranch, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
