#ifndef BP_H
#define BP_H


class BranchPredictor
{
public:
    BranchPredictor() { }
    virtual BOOL makePrediction(ADDRINT address) { return FALSE;};
    virtual void makeUpdate(BOOL takenActually, BOOL takenPredicted, ADDRINT address) {};
};

#endif
