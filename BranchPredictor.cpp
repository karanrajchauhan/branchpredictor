#ifndef BP_H
#include "BranchPredictor.h"
#endif

class myBranchPredictor: public BranchPredictor
{
public:
    myBranchPredictor() {}
    BOOL makePrediction(ADDRINT address){ return FALSE; }
    void makeUpdate(BOOL takenActually, BOOL takenPredicted, ADDRINT address){}
};
