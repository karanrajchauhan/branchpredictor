#ifndef ALL_BP_H
#define ALL_BP_H

#ifndef BP_H
#include "BranchPredictor.h"
#endif

class AllTakenBP: public BranchPredictor
{
public:
    AllTakenBP();
    BOOL makePrediction(ADDRINT address);
    VOID makeUpdate(BOOL takenActually, BOOL takenPredicted, ADDRINT address) {};
};

#endif
